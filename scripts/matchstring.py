
#import sys
#str1 = sys.argv[1]
#str2 = sys.argv[2]
#levenshtein(s1,s2)

def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = xrange(len(s2) + 1)
    for i, c1 in enumerate(s1):
        #print i , c1 , current_row
        current_row = [i + 1]
        #print i , c1 , current_row[0]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1       # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row

    return previous_row[-1]

def does_have ( s , l ):
    for sts in l :
        length = len(s)
        diff = levenshtein(s,sts)
        ratio = float(diff)/float(length)
        #print ratio
        if ( ratio < 0.3 ):
            l.remove(sts)
            return True
    return False


def intersection (l1 , l2):
    intersect = []
    for token in l1 :
        #sts = does_have(token , l2 )
        if does_have(token , l2 ) :
            intersect.append(token)
    return intersect


def similarity(s1 , s2):
    str1 = s1.lower()
    str2 = s2.lower()
    str1 = str1.replace('(' , ' ')
    str2 = str2.replace('(', ' ')
    str1 = str1.replace(')' , ' ')
    str2 = str2.replace(')', ' ')
    str2 = str2.replace('-', ' ')
    str2 = str2.replace('-', ' ')
    tokens1 = str1.strip().split()
    tokens2 = str2.strip().split()
    length = len(tokens2)
    intersect = intersection(tokens1,tokens2)
#print tokens1
#print length , len(intersect)
    ratio = float( len(intersect))/float(length )
    #print ratio
    return ratio
    #if ratio > 0.55 :
     #  print "True"
#print intersect
#print levenshtein(s1,s2)

