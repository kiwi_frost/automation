import urllib,csv,os
from urlparse import urljoin
from subprocess import call
import urllib2
import httplib
import cookielib,sys
from lxml.html import fromstring
from lxml import etree
import sys,traceback

BASE_URL = "http://en.wikipedia.org/"

class Crawl():
	content = ''
	cj = cookielib.CookieJar()
	html=''
	tree=''
	links=[]
	
	def fetch(self, url,values=''):
		opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cj))
		data=urllib.urlencode(values);
		req=urllib2.Request(url,data);
		##CookieJar.extract_cookies(response, request);
		response = opener.open(req);
		self.content = response.read();


	def wget(self,url):
		if len(sys.argv) == 4 and sys.argv[3]=='file':
			pass
		else:
			call(["wget",url])

	def set_html(self, fileName=None):
		if fileName is None:
			f=open(sys.argv[1].split('/')[-1],'r')
		else:
			try:
				print fileName
				f=open(fileName,'r')
			except IOError:
        			traceback.print_exc(file=sys.stdout)
				return
		html=f.read()
		self.tree = fromstring(html)
		return self.tree
			
	def getmovieTitle(self,movieName):
		if movieName is None or len(movieName)==0:
			return
		temp=movieName.split('(')[0]
		if temp is not None and temp[-1] == '_':
			return temp[:-1]
		return temp

	def get_songs(self,movieName=None,recurse=True):
		try:
		##print self.tree.text_content()
			filename=movieName+".info"
			movieInfo=open(sys.argv[2]+"/"+filename,'w')
			if recurse is True:
				movieTitle = self.getmovieTitle(movieName)
			else:
				movieTitle = movieName	##this is to avoid titles like:index.php?title=Valon_Ja_Varjon_Huoneet&action=edit&redlink=1.info
			if movieTitle is not None:
				movieInfo.write("Movie: " + movieTitle)
				
			success = False
			if recurse:	
				for a in self.tree.cssselect('a'):
					href=a.get('href')
					if href is not None and 'soundtrack' in href and movieTitle in href:
						href = urljoin(BASE_URL,href)
						print href
						call(['wget',href])
						self.set_html(href.split('/')[-1])
						self.get_songs(href.split('/')[-1],recurse=False)
				
				

			for a in self.tree.cssselect('.tracklist  tr'):
				success=True
				soundtrack=[]
				for td in a.cssselect('td'):
					soundtrack.append(td.text_content())
				for element in soundtrack:
					print element
					movieInfo.write(element.encode('utf-8').strip() + "~~")
				movieInfo.write("\n")
			if not success:
				for a in self.tree.cssselect('.wikitable tr'):
					success=True
					soundtrack=[]
					for td in a.cssselect('td'):
						soundtrack.append(td.text_content())
					for element in soundtrack:
						print element
						movieInfo.write(element.encode('utf-8').strip() + "~~")
					movieInfo.write("\n")
			if not success:
				for a in self.tree.cssselect('table'):
					heading =[]
					for th in a.cssselect('tr th'):
						heading.append(th.text_content())
					print heading
					if 'Title' in heading or 'Singers' in heading or 'Composer' in heading or 'Song' in heading or 'Singer(s)' in heading:
						print "am true"
						success=True
					else:
						continue
					for tr in a.cssselect('tr'):
						soundtrack=[]
						for td in tr.cssselect('td'):
							soundtrack.append(td.text_content())
						for element in soundtrack:
							print element
							movieInfo.write(element.encode('utf-8').strip() + "~~")
						movieInfo.write("\n")

			if not success:
				for a in self.tree.cssselect('ol'):
					soundtrack=[]
					for li in a.cssselect('li'):
						line=li.text_content()
						if '^' not in line:
							soundtrack.append(line)
					for element in soundtrack:
						print element
						movieInfo.write(element.encode('utf-8').strip() + "~~")

					movieInfo.write("\n")
			movieInfo.close()
		
		except IOError:
				traceback.print_exec(file=sys.stdout)
					
	def get_links(self):
		try:
			print "here"
			print self.tree.body is None
			print dir(self.tree)
			##print self.tree.text_content()
			for a in self.tree.cssselect('.wikitable td i a'):
				print "here"
				href=a.get('href')
				cssClass = a.get('class');
				name=a.text_content();
				if href is not None:            
					print href
					if  not href.startswith('http://'):
						href = urljoin(BASE_URL,href)
						pass
		    
					if href not in self.links:
						self.links.append(href);
		except:
			print "some error";
        		traceback.print_exc(file=sys.stdout)
		
		return self.links;

	def crawlmovies(self,moviesList):
		for movie in moviesList:
			c.wget(movie)
			fileName=movie.split('/')[-1]
			print movie
			c.set_html(fileName)
			c.get_songs(fileName)

if __name__ == "__main__":
	c=Crawl()
	c.wget(sys.argv[1])
	c.set_html(sys.argv[1].split('/')[-1])

	movies=c.get_links()
	
	if not os.path.exists(sys.argv[2]):
		os.makedirs(sys.argv[2])
	
	##movieName=sys.argv[1].split('/')[-1]
	##print movies
	'''movies=[]
	with open("movieList.csv",'r') as csvfile:
		f=csv.reader(csvfile,delimiter='\t')
		for line in f:
			for movie in line:
				movies.append(movie)'''
	c.crawlmovies(movies)

