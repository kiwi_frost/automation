#! /usr/bin/python
import urllib2
import sys
from bs4 import BeautifulSoup
filename = open(sys.argv[1],'r+')
flag = False
line  = filename.readline()
while line:
    try:
        tokens = line.split('^')
        web_page = urllib2.urlopen("http://www.songspk.name/" + tokens[1]).read()
        soup = BeautifulSoup(web_page,"html.parser")
        song_list = ''
        for tag in soup.find_all("a"):
            #if  tag.has_:
            if tag.has_key('href') and ('link.songspk.name' in tag['href'] or 'link1.songspk.name' in tag['href']):
                if tag.string is not None:
                    song_name = tag.string.encode('utf-8').strip()
                else :
                    song_name = tag.span.string.encode('utf-8').strip()
                final_song_name = ''
                for sub_text in song_name.splitlines():
                    final_song_name = final_song_name + " " + sub_text.strip()
                song_list = song_list + "^" + final_song_name.strip() + "^" + tag['href'].encode('utf-8')
        print tokens[0]  + song_list
        flag = False
        line = filename.readline()
    except:
        if flag == False :
            print "Got an Error !"
            flag = True
        else:
            print "Unsuccessfull in 2nd try ....  moving to next line"
            line = filename.readline()
            flag = False


