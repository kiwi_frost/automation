from redis import Redis
import csv
r=Redis()
KEY='songtolink'

if not r.exists(KEY):
	with open('songtolink.csv', 'rb') as csvfile:
		spamreader = csv.reader(csvfile, delimiter='\t')
		for row in spamreader:
			r.hset(KEY,row[0],row[1]);
