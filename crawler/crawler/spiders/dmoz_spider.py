from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
class DmozSpider(BaseSpider):
    name = "dmoz"
    allowed_domains = ["songspk.name"]
    start_urls = [
            "http://www.songspk.name/l_list.html",
            "http://www.songspk.name/m_list.html",
            "http://www.songspk.name/n_list.html",
            "http://www.songspk.name/o_list.html",
            "http://www.songspk.name/p_list.html",
            "http://www.songspk.name/q_list.html",
            "http://www.songspk.name/r_list.html",
            "http://www.songspk.name/s_list.html",
            "http://www.songspk.name/t_list.html",
            "http://www.songspk.name/u_list.html",
            "http://www.songspk.name/v_list.html",
            "http://www.songspk.name/w_list.html",
            "http://www.songspk.name/x_list.html",
            "http://www.songspk.name/y_list.html",
            "http://www.songspk.name/z_list.html"]

    def parse(self, response):
        filename = response.url.split("/")[-1]
        file1 = open(filename, 'wb')
        hxs = HtmlXPathSelector(response)
        td = hxs.select('//table')
        for a in td.select('.//a'):
            movie_name_ = a.select('text()').extract()
            link_ = a.select('@href').extract()
            if len(movie_name_) != 0 :
                movie_name = movie_name_[0].rstrip()
                print movie_name + " "
                movie_split = movie_name.splitlines()
                final_movie_name = ""
                for st in movie_split :
                    final_movie_name = final_movie_name + " " + st.strip()
                file1.write(final_movie_name.encode('utf-8') + "^")
            if len(link_) != 0 :
                link = link_[0].rstrip()
                print link + "\n"
                file1.write(link.encode('utf-8') + "\n")
        file1.close()



